#!/usr/bin/env python

from __future__ import annotations
import sys
import time
import json

import boto3
from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('LMConfig')

with open('config.json', 'r') as file:
    config = json.load(file)
    targets = config['targets']
    alert_profiles = config['alert_profiles']

table.delete_item(
        Key={
            'Type': 'alert_profile',
            'Version': 0
            }
        )
table.delete_item(
        Key={
            'Type': 'targets',
            'Version': 0
            }
        )
table.put_item(Item={
    'Type': 'alert_profiles',
    'Version': 0,
    'Definitions': alert_profiles
    })
table.put_item(Item={
    'Type': 'targets',
    'Version': 0,
    'Definitions': targets
    })


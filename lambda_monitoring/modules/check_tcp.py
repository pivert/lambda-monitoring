#!/usr/bin/env python
# Template doc:
# https://adamj.eu/tech/2021/10/09/a-python-script-template-with-and-without-type-hints-and-async/
#
# Author      : %USER%
# Date        :
# Description :
# License     :
# Created on  : %DATE%

from __future__ import annotations
import socket

def check(config: dict) -> dict:
    output: dict = {}
    output['success'] = False
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(5)
    r = sock.connect_ex((config['parsed_url']['host'], config['parsed_url']['port']))
    if r == 0:
        output['success'] = True
        output['msg'] = "TCP Connection OK"
    else:
        output['success'] = False
        output['msg'] = f"TCP Connection on port {config['parsed_url']['port']} failed"
    return output

#!/usr/bin/env python
# Template doc:
# https://adamj.eu/tech/2021/10/09/a-python-script-template-with-and-without-type-hints-and-async/
#
# Author      : François Delpierre
# Date        : 2022-03-26
# Description : SMTP Test
# License     : GNU GPL
# Created on  : 2022-03-26

from __future__ import annotations
import smtplib

LOCAL_HOSTNAME='pivert.org'

def check(config: dict) -> dict:
    output: dict = {}
    output['success'] = False
    port=config['parsed_url']['port'] if 'port' in config['parsed_url'].keys() else 25

    s = smtplib.SMTP(
        host=config['parsed_url']['host'],
        port=port,
        local_hostname=LOCAL_HOSTNAME,
        timeout=10.0)
    r = s.ehlo(LOCAL_HOSTNAME)
    if r[0] == 250 and config['keyword'] in str(r[1]):
        output['success'] = True
        output['msg'] = f"RC250 and keyword found : {r[1]}"
    else:
        output['success'] = False
        output['msg'] = str(r)
    return output

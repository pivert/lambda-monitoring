#!/usr/bin/env python
# Template doc:
# https://adamj.eu/tech/2021/10/09/a-python-script-template-with-and-without-type-hints-and-async/
#
# Author      : François Delpierre
# Date        : 2022-03-26
# Description : SMTP Test
# License     : GNU GPL
# Created on  : 2022-03-26

from __future__ import annotations
import smtplib
import ssl, socket, datetime

LOCAL_HOSTNAME='pivert.org'

def check(config: dict) -> dict:
    output: dict = {}
    output['success'] = False
    ssl_context = ssl.create_default_context();

    if ('tls_insecure' in config.keys() and config['tls_insecure']):
        ssl_context.check_hostname=False
        ssl_context.verify_mode=ssl.CERT_NONE
        output['cert_check'] = False
    else:
        output['cert_check'] = True

    port=config['parsed_url']['port'] if 'port' in config['parsed_url'].keys() else 465

    s = smtplib.SMTP_SSL(
        context=ssl_context,
        host=config['parsed_url']['host'],
        port=port,
        local_hostname=LOCAL_HOSTNAME,
        timeout=10.0)
    r = s.ehlo(LOCAL_HOSTNAME)
    if r[0] == 250 and config['keyword'] in str(r[1]):
        output['success'] = True
        output['msg'] = str(r[1])
    else:
        output['success'] = False
        output['msg'] = f"Keyword |{config['keyword']}| does not match server string |{str(r[1])}|"



    output['days_to_expiration'] = 0
    with socket.create_connection((config['parsed_url']['host'], config['parsed_url']['port'])) as sock:
        with ssl_context.wrap_socket(sock, server_hostname=config['parsed_url']['host']) as ssock:
            certificate = ssock.getpeercert()
            if 'notAfter' in certificate.keys():
                # Format : May 22 20:18:33 2022 GMT
                cert_expires = datetime.datetime.strptime(certificate['notAfter'], '%b %d %H:%M:%S %Y %Z')
                output['days_to_expiration'] = (cert_expires - datetime.datetime.now()).days

    return output

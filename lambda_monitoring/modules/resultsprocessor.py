#!/usr/bin/env python
# cSpell:includeRegExp /".*",/
# cSpell:includeRegExp /\(f?".*"/
#

from threading import Thread
from queue import Empty, Queue
import boto3
import logging
from boto3.dynamodb.conditions import Key
from datetime import datetime
from modules.opsgenie import Opsgenie

class ResultsProcessorThread(Thread):
    """
    Watch the resultsq queue for results.
    Store them in DynamoDB and check against check config.
    Update Opsgenie accordingly
    """
    def __init__(self, profile_name: str, profile_details: dict):
        """
        Initialize the Dynamo DB connection and the input queue
        """
        Thread.__init__(self)
        self.resultsq = Queue(15)
        self._running = True
        self.dynamodb = boto3.resource('dynamodb')
        self.table = self.dynamodb.Table('LMData')  # type: ignore
        self.name = f"ResultsProcessor_{profile_name}"
        self.profile_details = profile_details
        self.counter = 0
        self.opsgenie = Opsgenie(profile_details)
        self.logger = logging.getLogger(self.name)
        self.logger.info(f"Thread initialized")

    def check_alert_rules(self, current_check_result:dict):
        '''
        Check the results against the alert_profile and send notification on change
        '''
        # alert_profile_details = self.conf.alert_profiles[last_check_result['config']]
        assert isinstance(self.profile_details, dict)
        check_name = current_check_result['CheckName']

        # Take the last 'sample' results from DB (the current one has not yet been written)
        previous_check_results = self.table.query(
            KeyConditionExpression=Key('CheckName').eq(check_name),
            Limit=int(self.profile_details['sample']),
            ScanIndexForward=False
        )

        # Add the results in front of the Previous Check Result List
        check_results = [current_check_result] + list(previous_check_results['Items'])


        # Check for the minimum required success (or false) results.
        success_list: list[bool] = [val['success'] for val in check_results]
        success_count: int = sum(success_list)
        if success_count < self.profile_details['required']:
            # The service does not meet the minimum successes requirements over the last checks
            # We must notify Opsgenie
            current_check_result['min_success_required'] = False

            if ('notify' in self.profile_details.keys() 
                    and self.profile_details['notify']):
                # Always notify when failing
                request_id: str = self.opsgenie.alert(
                    check_result=current_check_result,
                    success_count=success_count
                )
                self.logger.info(f"{check_name} : Test Failed - Opsgenie request_id: {request_id}")

        else:
            # Service is OK
            current_check_result['min_success_required'] = True
            # Always notify to keep last state update in Opsgenie
            # FIXME: We could only notify in case of state change
            if ( 'min_success_required' in check_results[1].keys()
                    and 'min_success_required' in check_results[0].keys()
                    and check_results[0]['min_success_required'] != check_results[1]['min_success_required']):
                self.opsgenie.recovery(check_name)
            else:
                self.logger.info(f"{check_name} test is still OK - Won't notify recovery to Opsgenie")


    def run(self) -> None:
        self.logger.debug(f"Starting thread")
        # Continue running even if terminated to process remaining results
        while self._running or not self.resultsq.empty():
            try:
                check_result = self.resultsq.get(timeout=1)
                self.logger.debug(str(check_result))
                self.counter += 1
                if 'CheckName' in check_result.keys():
                    self.logger.debug(f"{check_result['CheckName']} - check alerts & insert results into table")
                    # check_alert_rules checks for alerts, and enrich check_results with notification_id if any
                    self.check_alert_rules(check_result)
                    self.table.put_item(Item=check_result)
                else:
                    self.logger.error('Invalid result : no "CheckName" key')
            except Empty:
                self.logger.debug(f"resultsq queue is empty")
        self.logger.info(f"Stopped after processing a total of {self.counter} results set.")


    def terminate(self) -> None:
        self.logger.info(
            f"Stopping after processing {self.counter} elements - processing remaining queued items"
        )
        self._running = False

#!/usr/bin/env python
# Author      : François Delpierre
# Date        : 19/03/2022
# Description : Dummy check - to be used as example for new checks
# License     : GNU GPL
# Created on  : 19/03/2022

from __future__ import annotations
import ssl, socket, datetime
from urllib.error import HTTPError, URLError
import urllib.request as req

def check(config: dict) -> dict:
    output: dict = {}
    output['success'] = False
    ssl_context = ssl.create_default_context();

    if ('tls_insecure' in config.keys() and config['tls_insecure']):
        ssl_context.check_hostname=False
        ssl_context.verify_mode=ssl.CERT_NONE
        output['cert_check'] = False
    else:
        output['cert_check'] = True

    try:
        site = req.urlopen(config['url'], context=ssl_context, timeout=5)
    except HTTPError as e:
        output['msg'] = f"{config['name']} HTTP Error : {e}"
        
    except URLError as e:
        output['msg'] = f"{config['name']} Certificate verification failed. URLError : {e}"
        output['success'] = False
    else:
        if site.msg == 'OK':
            site_str = site.read().decode('utf-8')
            if 'keyword' in config.keys():
                if config['keyword'] in site_str:
                    output['msg'] = ( f"{config['name']} - Keyword \'{config['keyword']}\' found" )
                    output['success'] = True
                else:
                    output['msg'] = ( f"{config['name']} Keyword \'{config['keyword']}\' not found")
                    output['success'] = False
            else:
                output['msg'] = ( f"{config['name']} - Response code : {site.code}" )
                output['success'] = True

            # Check Certificate Expiry
            output['days_to_expiration'] = 0
            with socket.create_connection((config['parsed_url']['host'], config['parsed_url']['port'])) as sock:
                with ssl_context.wrap_socket(sock, server_hostname=config['parsed_url']['host']) as ssock:
                    certificate = ssock.getpeercert()
                    if 'notAfter' in certificate.keys():
                        # Format : May 22 20:18:33 2022 GMT
                        cert_expires = datetime.datetime.strptime(certificate['notAfter'], '%b %d %H:%M:%S %Y %Z')
                        output['days_to_expiration'] = (cert_expires - datetime.datetime.now()).days

        else:
            output['msg'] = ( f"{config['name']} - Problem : {site.code}" )
            output['success'] = False


    return output

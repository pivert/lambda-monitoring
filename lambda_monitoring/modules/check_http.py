#!/usr/bin/env python
# Author      : François Delpierre
# Date        : 19/03/2022
# Description : Check http & https schemes
# License     : GNU GPL
# Created on  : 19/03/2022

from __future__ import annotations
import ssl
from urllib.error import HTTPError, URLError
import urllib.request as req

def check(config: dict) -> dict:
    output: dict = {}
    output['success'] = False
    ssl_context = ssl.create_default_context();
    if ('tls_insecure' in config.keys() and config['tls_insecure']):
        ssl_context.check_hostname=False
        ssl_context.verify_mode=ssl.CERT_NONE
    try:
        site = req.urlopen(config['url'], context=ssl_context, timeout=9)
    except HTTPError as e:
        output['msg'] = f"{config['name']} HTTP Error : {e}"
        
    except URLError as e:
        output['msg'] = f"{config['name']} Certificate verification failed. URLError : {e}"
        output['success'] = False
    else:
        if site.msg == 'OK':
            site_str = site.read().decode('utf-8')
            if 'keyword' in config.keys():
                if config['keyword'] in site_str:
                    output['msg'] = ( f"{config['name']} - Keyword \'{config['keyword']}\' found" )
                    output['success'] = True
                else:
                    output['msg'] = ( f"{config['name']} Keyword \'{config['keyword']}\' not found")
                    output['success'] = False
            else:
                output['msg'] = ( f"{config['name']} - Response code : {site.code}" )
                output['success'] = True
        else:
            output['msg'] = ( f"{config['name']} - Problem : {site.code}" )
            output['success'] = False
    return output

#!/usr/bin/env python
# Author      : François Delpierre
# Date        : 19/03/2022
# Description : Dummy check - to be used as example for new checks
# License     : GNU GPL
# Created on  : 19/03/2022

# check function is executed in thread or process with the config parameter
# that looks like:
# config['url'] = <scheme>://<host>:<port>/<folder>
# config['name'] = a name for the test (optional)
#   parsed_url:
#     scheme: <scheme>
#     host: <host>
#     port: <port>
#   host: <host> # optional, relevant if different from the host in url

from __future__ import annotations

def check(config: dict) -> dict:
    return {
        'msg': f"Hello from dummy. Parameters: \
        {config['parsed_url']['host']}:{config['parsed_url']['port']}",
        'success': True
    }

#!/usr/bin/env python
#
# Author      : François Delpierre
# Date        :
# Description : Read & parse config from database
# License     : GNU GPL
# Created on  : %DATE%

from __future__ import annotations

import logging
import re
import socket
import sys

import boto3
from boto3.dynamodb.conditions import Key

logger = logging.getLogger()


class Config:
    def __init__(self) -> None:
        '''
        Read cfg_file as JSON.
        Validate and enrich config by parsing data like url.

        Parameters:
            cfg_file: the path of the JSON formated config file containing a 'targets' document

        Returns:
            None
        '''

        # Get profile configs and targets from DB
        self.table = boto3.resource('dynamodb').Table('LMConfig')
        self.alert_profiles = self.table.query(KeyConditionExpression=Key(
            'Type'
        ).eq(
            'alert_profiles'
        ), ScanIndexForward=False, Limit=1)
        try:
            self.alert_profiles = self.alert_profiles['Items'][0]['Definitions']
        except IndexError:
            logger.error("No alert profile found")
            sys.exit(1)

        self.targets = self.table.query(KeyConditionExpression=Key(
            'Type'
        ).eq(
            'targets'
        ), ScanIndexForward=False, Limit=1)
        self.targets = self.targets['Items'][0]['Definitions']

        # Pre-compile the regex used in enrich_and_validate
        self.regex = re.compile(r'''
                           (?P<scheme>\w+)
                           ://(?P<host>[-\w.]+)(?::(?P<port>\d+))?
                           (?P<path_and_query>\/.*)?
                           ''', re.VERBOSE)

        # Validate each target config (set 'valid' bool) and enrich it with missing parameter defaults
        for target in self.targets:
            self.enrich_and_validate(target)  # target is mutable
            if target['valid']:
                logger.debug(f"Config for {target['name']} is valid")
            else:
                logger.info(f"Config for {target['name']} is invalid")

    def enrich_and_validate(self, target: dict) -> None:
        '''
        Check target config and decode the url as
        a parsed_url dictionary.
        '''
        target['valid'] = False
        target['parsed_url'] = dict()

        m = re.match(self.regex, target['url'])

        if m:
            params = m.groupdict()
            target['parsed_url']['scheme'] = params['scheme']
            if params['port']:
                target['parsed_url']['port'] = int(params['port'])
                target['parsed_url']['host'] = params['host']
                target['valid'] = True
            else:
                try:
                    target['parsed_url']['port'] = socket.getservbyname(
                        target['parsed_url']['scheme']
                    )
                    target['parsed_url']['host'] = params['host']
                    target['valid'] = True
                except:
                    logger.warning(f"Invalid scheme: {target['parsed_url']['scheme']} in {target['url']}")
        else:
            target['parsed_url']['msg'] = f"Failed to parse url {target['url']} - should match \
                the form <scheme>://<hostname>:<optional_port>/<optional_path>"

        # Ensure each target has an alert_profile
        # and fall back to default alert_profile
        if 'alert_profile' in target.keys():
            if target['alert_profile'] not in self.alert_profiles:
                logger.error(f"{target['alert_profile']} profile does not exist in alert_profiles")
                target['valid'] = False
        else:
            target['alert_profile'] = 'default'

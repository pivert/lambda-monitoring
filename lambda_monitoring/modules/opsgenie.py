#!/usr/bin/env python
# cSpell:includeRegExp /".*",/
# cSpell:includeRegExp /\(f?".*"/
#

import logging
import opsgenie_sdk
from opsgenie_sdk.rest import ApiException


class Opsgenie:
    '''
    Opsgenie instance (one for each profile)
    Not threaded, will be a companion instanciated by each ResultsProcessorThread
    '''
    def __init__(self, conf, name: str = 'OpsGenie alert manager'):
        """
        Initialize the OpsGenie connection and the input queue
        """
        # self.notify: Queue[dict] = Queue(5)

        self.opsgenie_key: str = conf['opsgenie_key']
        self._running: bool = True
        self.name: str = name
        self.conf = conf
        self.counter: int = 0

        self.logger = logging.getLogger(__name__)

        # create an instance of the API class
        configuration = opsgenie_sdk.Configuration()
        configuration.api_key['Authorization'] = self.opsgenie_key
        self.api_instance = opsgenie_sdk.AlertApi(opsgenie_sdk.ApiClient(configuration))
        self.logger.info(f"Opsgenie Key : {self.opsgenie_key}")

    def alert(self, check_result: dict, success_count: int) -> str:
        '''Send AlertPayload to Opsgenie'''
        self.counter += 1
        request_id = "Error"
        if 'CheckName' in check_result.keys():
            # Create alert payload
            message = (
                f"{check_result['CheckName']} failed: "
                f"success ({success_count}) < min_required ({self.conf['required']})"
            )

            description = (
                f"{check_result['CheckName']} test failed: "
                f"success ({success_count}) < min_required ({self.conf['required']}) "
                f"from {self.conf['sample']} samples.\nmsg: {check_result['msg']}"
            )

            create_alert_payload = opsgenie_sdk.CreateAlertPayload(
                    source='LM',
                    message=message,
                    alias=f"{check_result['CheckName']}",
                    description=description,
                    priority='P2',
                    )

            # Send payload
            try:
                # Create Alert
                api_response = self.api_instance.create_alert(create_alert_payload)
                self.logger.info((
                    f"Opsgenie new request_id {api_response.request_id} "
                    f"for {check_result['CheckName']} failed service"
                ))
                request_id = api_response.request_id
            except ApiException as e:
                self.logger.error(
                    "Exception in {self.name} when calling AlertApi->create_alert: %s\n" % e
                )

        else:
            self.logger.error('Error in {self.name}: check_result has no "CheckName" key')

        return request_id

    def recovery(self, identifier: str) -> None:
        self.logger.info(f"Send Recovery to OpsGenie for request_id {identifier}")
        identifier_type = 'alias'
        close_alert_payload = opsgenie_sdk.CloseAlertPayload()

        try:
            self.api_instance.close_alert(
                identifier_type=identifier_type,
                identifier=identifier,
                close_alert_payload=close_alert_payload
            )
        except opsgenie_sdk.ApiException as e:
            self.logger.error(f"Could not send {identifier} recovery to Opsgenie : {e}")

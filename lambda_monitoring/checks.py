#!/usr/bin/env python

# Author      : François Delpierre
# Date        : 19/03/2022
# Description : Check http & tcp connections
# License     : GNU GPL
# Created on  : 13/03/2022
# cSpell:disable

from __future__ import annotations
from collections.abc import Sequence
from datetime import datetime
import importlib
import json
import logging
import sys
import time
import re

from queue import Queue
from threading import Thread
from modules.config import Config
from modules.resultsprocessor import ResultsProcessorThread


# Initialize a global logger
logger:logging.Logger = logging.getLogger() 
streamHandler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


def run_check(target:dict, output:dict, resultsq: Queue) -> dict:
    '''
    The heart of monitoring.
    Dynamically load the check_<checktype>.py file, and
    run the check() function from it in a dedicated thread

    Parameters:
        config: The definition of the check to run
        output: a dict that will be used to return the result
            when thread finish its check.
        verbosity: Optional verbosity level.
    '''

    # Initialize output dict for the test results
    # Assume failure if  not explicitely set
    logger.debug(f"Checking {target['name']}")
    output['DateTime'] = datetime.utcnow().isoformat()
    output['CheckName'] = f"{target['name']}"
    output['scheme'] = f"{target['parsed_url']['scheme']}"
    time_start_ms = time.time_ns() // 1000**2
    output['msg'] = "Unexpected error"
    output['success'] = False


    # Sanity check : only url is required for a config check.
    if 'url' not in target.keys():
        output['msg'] = "Check config is missing the mandatory 'url' parameter."
        return output

    # Import module for the check (like check_https.py)
    check_module_path = f"modules.check_{target['parsed_url']['scheme']}"
    try:
        check_module = importlib.import_module( check_module_path )
        logger.debug(f"Imported module : {target['parsed_url']['scheme']}")
    except Exception as e:
        logger.warning(f"Module {target['parsed_url']['scheme']} not found")
        output['msg'] = str(e)
        return output

    logger.debug(f"Starting {target['name']} with scheme {target['parsed_url']['scheme']}")

    # Run the test from the dynamically loaded module.
    try:
        check_result = check_module.check(target)
        logger.debug(f"Result for {target['name']} : {check_result}")
        output.update(check_result)

    except Exception as e:
        logger.error(f"Error from {check_module_path} with target {target}: " + str(e))
        output['msg'] = str(e)
        resultsq.put(output)

    else:
        if not isinstance(check_result, dict):
            output['msg'] += f"Check returned a {type(check_result)} instead of a dictionary"

        if 'msg' not in check_result.keys():
            output['msg'] += f"No output msg from check_{target['parsed_url']['scheme']}.py"

        elif 'success' not in check_result.keys():
            output['msg'] += f"'success' key is missing in check_{target['parsed_url']['scheme']}.py return dict"

    # The rest are non blocking checks, so we can record exec_duration_ms
    time_end_ms = time.time_ns() // 1000**2
    output['exec_duration_ms'] = time_end_ms - time_start_ms

    # We have a result with at least a 'msg' key in a dict
    # Send thread safe message to ResultsProcessor
    resultsq.put(output)

    # We passed all the checks. We can send function results
    return output


def start(regex_filter='.*'):
    # Started by the main or the lambda_handler
    start_time = time.time()
    conf = Config()             # load configuration
    out = dict()                # empty dict to gather all outputs
    check_threads = list()      # threads used for checks (with timeout)
    rp_threads = list()           # ResultProcessors

    # Start an results_processor instance for each profile.
    # An Opsgenie instance will be part of each results_processor
    # This must be started before checks on targets, because it provides the resultsq Queue
    # and store it into the check profile.
    for profile_name, profile_conf in conf.alert_profiles.items():
        # Sanitize
        if 'notify' not in profile_conf.keys():
            profile_conf['notify'] = True
        
        if 'sample' not in profile_conf.keys():
            profile_conf['sample'] = 5

        if 'required' not in profile_conf.keys():
            profile_conf['required'] = 3

        if 'opsgenie_key' not in profile_conf.keys() or len(profile_conf['opsgenie_key']) < 32:
            logger.error(f"Invalid {profile_name}: opsgenie_key is missing.")
            continue

        # Start a results_processor thread for each profile
        rp = ResultsProcessorThread(profile_name, profile_conf)
        rp_threads.append(rp)
        profile_conf['resultsq'] = rp.resultsq

    # Add the regex_filter to the profile confs
    urlsearch = re.compile(regex_filter)

    # Start each check in a dedicated threads
    for target in conf.targets:
        if target['valid'] and re.search(urlsearch, target['url']):
            # Prepopulate out variable. Those values should be overwriten in the test.
            alert_profile = conf.alert_profiles[target['alert_profile']]
            out[target['name']] = {
                'msg': 'Error starting asynchronously',
                'success': False,
            }
            th = Thread(
                name=f"{target['name']} {target['parsed_url']['scheme']}",
                target=run_check,
                args=(target, out[target['name']], alert_profile['resultsq'])
            )
            check_threads.append(th)
            th.start()

    logger.info(f"Main: {len(check_threads)} checkers threads started")



    # Release some CPU for all the checkers to start.
    time.sleep(0.5)

    for rp in rp_threads:
        rp.start()

    logger.info("ResultsProcessorThread profiles started")
    # Attach and wait for all checkers to finish
    for thread in check_threads:
        thread.join()

    for rp in rp_threads:
        rp.terminate()

    logger.debug(out)
    logger.info(f"Checks took {time.time() - start_time:.2f} seconds.")

    # Wait for each ResultProcessor threads to terminate
    for rp in rp_threads:
        while rp.is_alive():
            time.sleep(0.1)

    logger.info(f"Total execution time: {time.time() - start_time:.2f} seconds.")
    time.sleep(0.1)
    return 0


def main(argv: Sequence[str] | None = None) -> int:
    """ Start from command-line """
    import argparse

    parser = argparse.ArgumentParser(description="Run monitoring checks from config")
    parser.add_argument(
        '-v', '--verbose', action='count', dest='verbosity', default=0,
        help='Verbose mode (add multiple v to increase verbosity)'
    )
    parser.add_argument(
        '-f', '--regex-filter', default='.*',
        help='Filter check names by regex'
    )
    args = parser.parse_args(argv)

    # Logging
    logging_level = [
        'CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG', 'NOTSET'
    ][int(args.verbosity)]
    print(f"Logging level : {logging_level}")
    logger.setLevel(logging_level)
    return start(regex_filter=args.regex_filter)


## The process is either started by lambda or by command line (__main__)
def lambda_handler(event, context) -> dict:  # type: ignore
    """ Start from aws lambda """
    logger.setLevel(logging.INFO)
    out = start()
    return {
        'statusCode': 200,
        'body': json.dumps(out, indent=4),
    }


if __name__ == "__main__":
    raise SystemExit(main())

#!/usr/bin/env bash
python --version | grep -F '3.9' && sam local invoke -v ~/LocalDocuments/lambda-monitoring/.aws-sam/build --container-host 172.17.0.1 --container-host-interface 0.0.0.0 || echo "Ensure you have python 3.9"

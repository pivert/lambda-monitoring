# lambda-monitoring

This project contains source code and supporting files for a serverless application that you can deploy with the SAM CLI. It includes the following files and folders.

This lambda function & DynamoDB templates does the following:
- Read config from the LMConfig table
- Execute the configured checks one by one, in parallel, each in it's dedicated thread:
  - Try to load a python file named with the scheme. So an https://... check will load check_https.py
  - Start a thread with the check_<scheme>.py and pass the config portion
- As the thread finishes and send messages to a single db_writer thread via queue, write the results to a Dynamodb LMData table with a ttl for auto expiry of records.
